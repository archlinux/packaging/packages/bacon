# Maintainer: George Rawlinson <grawlinson@archlinux.org>
# Maintainer: Caleb Maclennan <caleb@alerque.com>
# Maintainer: Orhun Parmaksız <orhun@archlinux.org>
# Contributor: Jokler <aur@jokler.xyz>

pkgname=bacon
pkgver=3.11.0
pkgrel=2
pkgdesc='A background rust code checker'
arch=('x86_64')
url='https://dystroy.org/bacon'
license=('AGPL-3.0-only')
depends=('alsa-lib' 'glibc' 'gcc-libs')
makedepends=('git' 'rust')
optdepends=('bash-completion: enable bash completions'
            'cargo: for use with Rust'
            'clang: for use with C++ via clang'
            'gcc: for use with C++ via gcc'
            'eslint: for use with JavaScript'
            'python: for use with Python via unittest'
            'python-pytest: for use with Python via pytest')
options=('!lto')
source=("$pkgname::git+https://github.com/Canop/bacon#tag=v$pkgver")
sha512sums=('94ce604fc2f0e02ad6b3519da2d6e6f65a83e90d11bf0e1c3dee7e8065fa39ec62466404e0e409b7ee58392df09252381d597296928802316e3494411c2fcf12')
b2sums=('bcbeb6943550c7f6c655d2546e608cb2a61049a36a0dd4f830ac919aea804c59b1af8132434f539a41930039bacc1e4fac623817d587315ed5ab692a88b43f91')

prepare() {
  cd "$pkgname"

  # download dependencies
  cargo fetch --locked --target "$(rustc -vV | sed -n 's/host: //p')"
}

build() {
  cd "$pkgname"

  cargo build --frozen --release --all-features
}

check() {
  cd "$pkgname"

  cargo test --frozen --all-features
}

package() {
  cd "$pkgname"
  depends+=(libasound.so)

  local _binary="target/release/$pkgname"

  # binary
  install -vDm755 -t "$pkgdir/usr/bin" "$_binary"

  # documentation
  install -vDm644 -t "$pkgdir/usr/share/doc/$pkgname" ./*.md
  cp -vr doc img "$pkgdir/usr/share/doc/$pkgname"

  # completions
  install -vDm644 <(COMPLETE=bash   "$_binary") "$pkgdir/usr/share/bash-completion/completions/$pkgname"
  install -vDm644 <(COMPLETE=zsh    "$_binary") "$pkgdir/usr/share/zsh/site-functions/_$pkgname"
  install -vDm644 <(COMPLETE=fish   "$_binary") "$pkgdir/usr/share/fish/vendor_completions.d/$pkgname.fish"
  install -vDm644 <(COMPLETE=elvish "$_binary") "$pkgdir/usr/share/elvish/lib/$pkgname.elv"

  # license
  install -vDm644 -t "$pkgdir/usr/share/licenses/$pkgname" LICENSE
}
